const fs = require('fs');
const csv =  require('fast-csv');
const admin = require('firebase-admin');
const serviceAccount = require("./smetCertificate.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://smet-7e509.firebaseio.com"
});

let csvStream = csv.createWriteStream({headers: true}),
    writableStream = fs.createWriteStream("smetDBFebrero.csv");

csvStream.pipe(writableStream);
// instancia de la base de datos
const db = admin.firestore();
// leer datos de la base de datos
db.collection('Dispositivo').where("fecha",">=",201902000000).where("fecha","<",201903000000).orderBy("fecha","asc").get()
    .then((snapshot) => {
      snapshot.forEach((doc) => {
        csvStream.write({fecha: parseInt(doc.data().fecha), 
            temperatura: parseFloat(doc.data().medicion),
            humedad: parseFloat(doc.data().humedad)});
        //console.log(doc.id, '=>', doc.data());
        
        //console.log(typeof(doc.data().humedad));
        /* if(typeof(doc.data().humedad)==="undefined"){
            
        }
        else{
            csvStream.write({fecha: parseInt(doc.data().fecha), temperatura: parseFloat(doc.data().temperatura)});
        } */
        
        
      });
      csvStream.end();
    })
    .catch((err) => {
      console.log('Error getting documents', err);
    });


 
//or
 
